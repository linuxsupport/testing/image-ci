#!/bin/bash
# The following script runs Puppet tests

function t_Log {
  echo -e "[+] $(date) -> $*"
}

function t_CheckExitStatus {
  [ $1 -eq 0 ] && { t_Log "PASS"; return 0; }
  t_Log "FAIL"
  return 1
}

yum install -y bc

# Now run Puppet up to MAX_RUNS times. At some point before that, it needs to return
# status code 0 indicating that there were no changes.
MAX_RUNS=5
COUNT=0
RET=1
while (( $(echo "$COUNT <= $MAX_RUNS" |bc -l) )); do
  COUNT=$(bc <<< $COUNT+1)
  t_Log "Running puppet agent #$COUNT"
  puppet agent -t --masterport 8154
  RET=$?
  t_Log "(exit code was $RET)"
  t_CheckExitStatus $RET
  [[ $RET -eq 0 ]] && break

  if [[ -f /opt/puppetlabs/puppet/cache/state/agent_catalog_run.lock ]]; then
    # Oops, puppet is already running
    # Let's not count that attempt as a full one
    COUNT=$(bc <<< $COUNT-0.5)
    # Wait up to 10 minutes for the other puppet run to finish
    WAIT_COUNT=0
    while [[ -f /opt/puppetlabs/puppet/cache/state/agent_catalog_run.lock && $WAIT_COUNT -le 20 ]]; do
      t_Log "Puppet already running, waiting for it to finish..."
      WAIT_COUNT=$(($WAIT_COUNT+1))
      sleep 30s
    done
  fi
done
[[ $RET -ne 0 ]] && exit $RET

# Now run the Config Team's Puppet tests
git clone https://$PUPPET_TEST_TOKEN@gitlab.cern.ch/ai-config-team/cern_puppet_centos_functional_tests.git
cd cern_puppet_centos_functional_tests
git log -1

./runtests.sh
