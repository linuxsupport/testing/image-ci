#!/bin/bash

source common.sh

function usage {
  cat << EOF
Usage: ${0####*/} [-hp] -d OS_DISTRO -i IMAGE [-f FLAVOR] [-a ARCH] [-g CUSTOM_GIT_REPO] [-s CUSTOM_SCRIPT]
Test an Openstack image.

    -h                 Display this help and exit
    -d OS_DISTRO       Distro to test. Possible values C, CC, CS, RHEL, ALMA.
    -m OS_MAJOR        Distro major to test: 7, 8, etc.
    -i IMAGE           Openstack image to test.
    -f FLAVOR          Openstack flavor to test. If not specified, test all available Ironic flavors.
    -a ARCH            Architecture to test. Default: x86_64.
    -g CUSTOM_GIT_REPO Git repository to clone that will contain a custom script.
    -s CUSTOM_SCRIPT   Custom script to execute.
    -p                 Create a Puppet-managed machine with ai-bs. Default: False

EOF
}

PUPPET=false
while getopts hpd:m:i:f:a:g:s: opt; do
  case $opt in
    h)
      usage
      exit 0
      ;;
    d)
      OS_DISTRO="$OPTARG"
      ;;
    m)
      OS_MAJOR="$OPTARG"
      ;;
    i)
      IMAGE="$OPTARG"
      ;;
    f)
      FLAVORS=("$OPTARG")
      ;;
    a)
      ARCH="$OPTARG"
      ;;
    g)
      CUSTOM_GIT_REPO="$OPTARG"
      ;;
    s)
      CUSTOM_SCRIPT="$OPTARG"
      ;;
    p)
      PUPPET=true
      ;;
    *)
      usage >&2
      exit 1
      ;;
  esac
done
shift "$((OPTIND-1))"   # Discard the options and sentinel --

[ -z "$OS_DISTRO" ] && usage && exit 1
[ -z "$OS_MAJOR" ] && usage && exit 1
[ -z "$IMAGE" ] && usage && exit 1
[ -z "$FLAVORS" ] && FLAVORS=()
[ -z "$ARCH" ] && ARCH=x86_64

CI_PIPELINE_ID="${CI_PIPELINE_ID:-manual}"
# Delete failures by default
[[ "${DELETE_FAILURES,,}" == "false" ]] && DELETE_FAILURES=false || DELETE_FAILURES=true
[[ "$PUPPET" == "true" ]] && TESTTYPE="puppet" || TESTTYPE="unmanaged"

# How often and how many times to retry creating a machine with a "busy" flavor
MIN_RETRY_TIME=300 # 5 minutes
MIN_RETRY_COUNT=72 # 5*72 => 6 hours

function isServerCreated() {
  local show=$(openstack server show "$1" -c status --format json)
  [ $? -eq 1 ] && return 3

  local status=$(echo "$show" | jq -r '.status')

  case $status in
    'ERROR')
      return 3 ;;
    'ACTIVE')
      return 0 ;;
    'BUILD' | 'PENDING')
      return 4 ;;
  esac

  return 3
}

function isServerDeleted() {
  show=$(openstack server show "$1" -c status --format json 2>/dev/null)
  [[ $? -eq 1 ]] && return 0

  return 4
}

function deleteServer() {
  local p_name="$1"
  local flavor="$2"
  local test="$3"
  local delete=true

  # Figure out if we should really delete or not
  local result=$(testGetResult ${flavor} ${test})
  if [[ "$DELETE_FAILURES" == false && $result -ne $TEST_PASS && $result -ne $TEST_SKIPPED || $result == "" ]]; then
    delete=false
  fi

  if [[ "$delete" == false ]]; then
    # Add a comment to the result
    testAddComments "${flavor}" "${test}" "Machine ${p_name} not deleted, available for debugging"
  else
    t_Log "Deleting ${p_name}"
    if [[ "$PUPPET" == "true" ]]; then
      cmd="ai-kill ${p_name}"
    else
      cmd="openstack server delete ${p_name}"
    fi
    t_BoldGreen "${cmd}"
    eval ${cmd}
    [[ $? -eq 1 ]] && return 0
    # Wait for the server to really be deleted. Retry every 10 seconds, up to 60 times => wait 10min
    waitFor 60 10 isServerDeleted ${p_name}
  fi
}

# Show the image info
t_Log "Testing the following ${OS_DISTRO} image:"
openstack image show "${IMAGE}" --format json
if [[ $? -ne 0 ]]; then
  # Unable to see the image
  t_Log "Unable to find image ${IMAGE}"
  exit 1
fi
echo

# Get list of Ironic flavors if we weren't asked to test a specific one. Randomize the list
# to help avoid collisions with other jobs, as far as possible.
if [[ ${#FLAVORS[@]} -eq 0 ]]; then
  while IFS= read -r LINE; do
    ID="$(echo $LINE | cut -d' ' -f1)"
    NAME="$(echo $LINE | cut -d' ' -f2)"
    PROPS="$(echo $LINE | cut -d' ' -f3-)"
    # TODO: select $ARCH flavors here
    if echo "$PROPS" | grep -q "cern:physical"; then
      FLAVORS+=("${NAME}")
    fi
  done <<< "$(openstack flavor list -c ID -c Name -c Properties --long --format value | shuf)"
fi

t_Log "Openstack flavors to test:"
for f in "${FLAVORS[@]}"; do
  t_Log "  ${f}"

  # Create entries for the tests we're going to run
  testCreate "${TESTTYPE}-${f}" "create"
  testCreate "${TESTTYPE}-${f}" "ssh_access"
  if [[ "$PUPPET" == "true" ]]; then
    testCreate "${TESTTYPE}-${f}" "puppet_tests"
  elif [[ ! -z $CUSTOM_GIT_REPO ]] && [[ ! -z $CUSTOM_SCRIPT ]]; then
    testCreate "${TESTTYPE}-${f}" "custom_tests_clone"
    testCreate "${TESTTYPE}-${f}" "custom_tests_script"
  else
    testCreate "${TESTTYPE}-${f}" "upstream_centos_tests"
    testCreate "${TESTTYPE}-${f}" "cern_centos_tests"
  fi
  testCreate "${TESTTYPE}-${f}" "summary"
done

# Poor man's FIFO queue
TOBETESTED=("${FLAVORS[@]}")
declare -A BUSY_FLAVORS
GLOBAL_RETURN=0

while [[ ${#TOBETESTED[@]} -gt 0 ]]; do
  # Pop the first flavor
  flavor=${TOBETESTED[0]}
  TOBETESTED=("${TOBETESTED[@]:1}")

  PREV_TIME=$(echo "${BUSY_FLAVORS[$flavor]}" | cut -d' ' -f1)
  COUNT=$(echo "${BUSY_FLAVORS[$flavor]}" | cut -d' ' -f2)

  # If we've already retried too much, give up
  if [[ $COUNT -gt $MIN_RETRY_COUNT ]]; then
    testAddComments "${TESTTYPE}-${flavor}" "create" "All machines of this type were busy"
    testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_SKIPPED
    # If we were only supposed to test this one flavor, let's fail the whole pipeline
    [[ ${#FLAVORS[@]} -eq 1 ]] && GLOBAL_RETURN=$((GLOBAL_RETURN+1))
    continue
  fi

  if [[ ! -z $PREV_TIME ]]; then
    testClearComments "${TESTTYPE}-${flavor}" "create"
    # Wait at least $MIN_RETRY_TIME before trying again. Add up to 30 seconds to avoid deadlocks
    DELAY=$(( $MIN_RETRY_TIME - ($(date +%s) - $PREV_TIME) + ($RANDOM % 30) ))
    if [[ $DELAY -gt 0 ]]; then
      t_Log "Waiting for $DELAY seconds..."
      sleep $DELAY
    fi
  fi

  p_name="$(mktemp -u "citest-${CI_PIPELINE_ID}-XXXXX" | tr '[:upper:]' '[:lower:]')"
  t_Log "Trying to create ${p_name}, a machine of flavor ${flavor}"
  if [[ "$PUPPET" == "true" ]]; then
    # Foreman may deadlock if we try to create too many machines at once, so wait a random bit
    DELAY=$(($RANDOM % 10))
    t_Log "Wait ${DELAY} seconds"
    sleep $DELAY
    cmd="ai-bs --nova-sshkey imageci --landb-responsible=lxsoft-admins --nova-property centos_test_cleanup=true --nova-property puppet_managed=true --nova-flavor '${flavor}' --nova-image '${IMAGE}' --foreman-hostgroup '${PUPPET_HOSTGROUP}' --foreman-environment '${PUPPET_ENVIRONMENT}' '${p_name}'"
    t_BoldGreen "${cmd}"
    testStart "${TESTTYPE}-${flavor}" "summary"
    testStart "${TESTTYPE}-${flavor}" "create"
    cmd_output=$(eval ${cmd} 2>&1)
  else
    cmd="openstack server create --key-name imageci --property landb-responsible=lxsoft-admins --property centos_test_cleanup=true --property puppet_managed=false --flavor '${flavor}' --image '${IMAGE}' '${p_name}'"
    t_BoldGreen "${cmd}"
    testStart "${TESTTYPE}-${flavor}" "summary"
    testStart "${TESTTYPE}-${flavor}" "create"
    cmd_output=$(eval ${cmd} -c id --format value 2>&1)
  fi
  if [[ $? -ne 0 ]]; then
    # Unable to create the server, remove it from the list
    t_Log "Unable to create server of flavor ${flavor}"
    testEnd "${TESTTYPE}-${flavor}" "create" $TEST_ERROR
    testAddComments "${TESTTYPE}-${flavor}" "create" "Unable to create server"
    # Add the error to the comments
    testAddComments "${TESTTYPE}-${flavor}" "create" "${cmd_output}"
    testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_FAIL
    # If we were only supposed to test this one flavor, let's fail the whole pipeline
    [[ ${#FLAVORS[@]} -eq 1 ]] && GLOBAL_RETURN=$((GLOBAL_RETURN+1))
    continue
  fi

  # Wait for the server to be ready or to fail. Retry every 60 seconds, up to 30 times => wait 30min
  waitFor 30 60 isServerCreated ${p_name}
  if [[ $? -eq 1 ]]; then
    t_Log "Timed out while creating a server of flavor ${flavor}"
    testEnd "${TESTTYPE}-${flavor}" "create" $TEST_ERROR
    testAddComments "${TESTTYPE}-${flavor}" "create" "Timed out while creating server"
    testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_FAIL
    deleteServer "${p_name}" "${TESTTYPE}-${flavor}" "create"
    # If we were only supposed to test this one flavor, let's fail the whole pipeline
    [[ ${#FLAVORS[@]} -eq 1 ]] && GLOBAL_RETURN=$((GLOBAL_RETURN+1))
    continue
  fi

  show=$(openstack server show ${p_name} -c status -c fault --format json)
  status=$(echo "$show" | jq -r '.status')
  error=$(echo "$show" | jq -r '.fault.message')

  if [[ $status == 'ERROR' ]]; then
    t_Log "Fault message: $error"
    testEnd "${TESTTYPE}-${flavor}" "create" $TEST_ERROR
    openstack server list
    # If the flavor is not a virtual one, show the state of the baremetal nodes
    [[ $flavor != m2.* ]] && openstack baremetal node list --long -c Name -c 'Instance UUID' -c 'Power State' -c 'Provisioning State' -c 'Maintenance' -c 'Resource Class'
    if [[ $error =~ "No valid host was found" ]]; then
      # Is this the first time we've tried this flavor?
      COUNT=$(echo "${BUSY_FLAVORS[$flavor]}" | cut -d' ' -f2)
      if [[ -z "$COUNT"  ]]; then
        # Yep, never tried it before.
        # There are either no machines of that hardware type available, or we're already testing all the ones we have
        # Let's check if we already have a machine of that hw type created
        existing=0
        while IFS= read -r LINE; do
          [[ -z "$LINE" ]] && break
          existing=1
          ID="$(echo $LINE | cut -d' ' -f1)"
          NAME="$(echo $LINE | cut -d' ' -f2)"
          t_Log " $NAME ($ID) already exists with that flavor"
        done <<< "$(openstack server list -c ID -c Name -c Flavor -c Status --format value | grep -v ERROR | grep -v "${p_name}" | grep " ${flavor}$")"
        if [[ $existing -eq 1 ]]; then
          # Yep, there's another machine, so we can try to test this flavor again later
          TOBETESTED+=("${flavor}")

          COUNT=$(echo "${BUSY_FLAVORS[$flavor]}" | cut -d' ' -f2)
          BUSY_FLAVORS[${flavor}]="$(date +%s) $(($COUNT+1))"
        else
          # No machine of the type currently exists
          # Do we even have quota for it?
          QUOTA=$(openstack project show "${OS_PROJECT_NAME}" -c max-instances_${flavor} --format value 2>/dev/null)
          [ -z "$QUOTA" ] && QUOTA="Unlimited"
          t_Log "Max instances for flavor ${flavor} in project '${OS_PROJECT_NAME}' is $QUOTA."
          if [[ $QUOTA > 0 || $QUOTA == "Unlimited" ]]; then
            # There's still hope, so we can try to test this flavor again later
            TOBETESTED+=("${flavor}")

            COUNT=$(echo "${BUSY_FLAVORS[$flavor]}" | cut -d' ' -f2)
            BUSY_FLAVORS[${flavor}]="$(date +%s) $(($COUNT+1))"
          else
            t_Log " No other machine found, maybe none are available with this flavor?"
            testAddComments "${TESTTYPE}-${flavor}" "create" "No machines of this type found"
            testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_FAIL
            # If we were only supposed to test this one flavor, let's fail the whole pipeline
            [[ ${#FLAVORS[@]} -eq 1 ]] && GLOBAL_RETURN=$((GLOBAL_RETURN+1))
          fi
        fi
      else
        # If we tried it before, that's because at some point we determined it was possible
        # to create a machine of this type, so keep trying
        TOBETESTED+=("${flavor}")

        COUNT=$(echo "${BUSY_FLAVORS[$flavor]}" | cut -d' ' -f2)
        BUSY_FLAVORS[${flavor}]="$(date +%s) $(($COUNT+1))"
      fi
    else
      # If we can't even create the machine, we've failed
      t_CheckExitStatus 1
      GLOBAL_RETURN=$((GLOBAL_RETURN+1))
      t_Log "Unexpected error while creating machine: $error"
      testAddComments "${TESTTYPE}-${flavor}" "create" "$error"
      testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_FAIL
    fi

    deleteServer "${p_name}" "${TESTTYPE}-${flavor}" "create"
  else
    testEnd "${TESTTYPE}-${flavor}" "create" $TEST_PASS
    t_Log "Machine created successfully!"
    openstack server show "${p_name}" --max-width 100

    t_Log "Trying to SSH into the machine"
    testStart "${TESTTYPE}-${flavor}" "ssh_access"
    # Wait for the machine to really be up. Retry every 30 seconds (+ 10 seconds SSH timeout), up to 30 times => wait 20min
    # For Puppet-managed machines, wait 60 min. (Puppet and distro-sync have to run too)
    [[ "$PUPPET" == "true" ]] && wait=90 || wait=30
    waitFor $wait 30 isServerUp "${p_name}"
    # If we can't SSH into the machine, we've failed
    g=$?
    GLOBAL_RETURN=$((GLOBAL_RETURN+g))
    t_CheckExitStatus $g
    if [[ $g -ne 0 ]]; then
      testEnd "${TESTTYPE}-${flavor}" "ssh_access" $TEST_FAIL
      testAddComments "${TESTTYPE}-${flavor}" "ssh_access" "Unable to SSH into the machine"
      testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_FAIL
      # Retrieve cloud-init logs in case it helps determining the problem with SSH access
      t_Log "Retrieve cloud-init logs"
      cloudInitLogsRetrieval "${p_name}"
      checkCommonErrors "${p_name}"
      deleteServer "${p_name}" "${TESTTYPE}-${flavor}" "ssh_access"
      continue
    fi
    testEnd "${TESTTYPE}-${flavor}" "ssh_access" $TEST_PASS

    # Upstream tests will fail too much for Puppet-managed machines, so instead for those we'll
    # check if Puppet ran cleanly.
    if [[ "$PUPPET" == "true" ]]; then
      t_Log "We're in! Running Puppet tests"
      testStart "${TESTTYPE}-${flavor}" "puppet_tests"
      runOnServer "${p_name}" "PUPPET_TEST_TOKEN="${PUPPET_TEST_TOKEN}" bash -s" < ./puppettests.sh
      g=$?
      GLOBAL_RETURN=$((GLOBAL_RETURN+g))
      t_CheckExitStatus $g

      t_Log "Retrieve cloud-init logs"
      cloudInitLogsRetrieval "${p_name}"

      if [[ $g -ne 0 ]]; then
        checkCommonErrors "${p_name}"
        testEnd "${TESTTYPE}-${flavor}" "puppet_tests" $TEST_FAIL
        testAddComments "${TESTTYPE}-${flavor}" "puppet_tests" "Puppet tests failed"
        testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_FAIL
      else
        testEnd "${TESTTYPE}-${flavor}" "puppet_tests" $TEST_PASS
        testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_PASS
      fi

      deleteServer "${p_name}" "${TESTTYPE}-${flavor}" "puppet_tests"
    elif [[ ! -z $CUSTOM_GIT_REPO ]] && [[ ! -z $CUSTOM_SCRIPT ]]; then
        t_Log "Custom git repo ${CUSTOM_GIT_REPO} passed, attempting clone"
	runOnServer "${p_name}" "dnf -y install git"
	runOnServer "${p_name}" "git clone $CUSTOM_GIT_REPO /tmp/custom_script"
	g=$?
        GLOBAL_RETURN=$((GLOBAL_RETURN+g))
        t_CheckExitStatus $g
	if [[ $g -ne 0 ]]; then
	  testEnd "${TESTTYPE}-${flavor}" "custom_tests_clone" $TEST_FAIL
	  testAddComments "${TESTTYPE}-${flavor}" "custom_tests_clone" "Unable to clone ${CUSTOM_GIT_REPO}"
        else
          testEnd "${TESTTYPE}-${flavor}" "custom_tests_clone" $TEST_PASS
	fi
	t_Log "Attempting execution of custom script: $CUSTOM_SCRIPT"
	runOnServer "${p_name}" "$CUSTOM_SCRIPT"
	g=$?
        GLOBAL_RETURN=$((GLOBAL_RETURN+g))
        t_CheckExitStatus $g
	if [[ $g -ne 0 ]]; then
          testEnd "${TESTTYPE}-${flavor}" "custom_tests_script" $TEST_FAIL
          testAddComments "${TESTTYPE}-${flavor}" "custom_tests_script" "Custom script $CUSTOM_SCRIPT failed"
        else
          testEnd "${TESTTYPE}-${flavor}" "custom_tests_script" $TEST_PASS
      fi
	t_Log "Retrieve cloud-init logs"
        cloudInitLogsRetrieval "${p_name}"
        deleteServer "${p_name}" "${TESTTYPE}-${flavor}" "custom_tests_script"
    else
      t_Log "We're in! Running upstream tests"
      # Find the OS we're running, and the commit hash of the latest passing test. Tested machines may not have external connectivity.
      centos_ver=$(runOnServer "${p_name}" "sed '/VERSION_ID/!d; s/.*\"\([0-9]\).*\"/\1/' /etc/os-release")
      if [[ "$centos_ver" -eq 9 ]]; then
        # CentOS 9 doesn't CI tests where we expect them, so we'll use the 8 ones
        centos_ver=8
      fi
      PASSING=$(curl -s https://ci.centos.org/job/CentOS-Core-QA-t_functional-c${centos_ver}-64/lastSuccessfulBuild/api/json | jq -r '.actions[] | select(.lastBuiltRevision) | .lastBuiltRevision.SHA1')
      testStart "${TESTTYPE}-${flavor}" "upstream_centos_tests"
      # Now run the actual tests
      runOnServer "${p_name}" "bash -s ${PASSING}" < ./upstreamcentostests.sh
      g=$?
      GLOBAL_RETURN=$((GLOBAL_RETURN+g))
      t_CheckExitStatus $g
      upstream_status=$g
      if [[ $g -ne 0 ]]; then
        testEnd "${TESTTYPE}-${flavor}" "upstream_centos_tests" $TEST_FAIL
        testAddComments "${TESTTYPE}-${flavor}" "upstream_centos_tests" "Upstream tests failed"
      else
        testEnd "${TESTTYPE}-${flavor}" "upstream_centos_tests" $TEST_PASS
      fi

      t_Log "Now run our own tests"
      testStart "${TESTTYPE}-${flavor}" "cern_centos_tests"
      if [[ $OS_DISTRO == 'RHEL' && "$centos_ver" -eq 7 ]]; then
        testAddComments "${TESTTYPE}-${flavor}" "cern_centos_tests" "CERN tests skipped as image is upstream RHEL"
        testEnd "${TESTTYPE}-${flavor}" "cern_centos_tests" $TEST_SKIPPED
        if [[ $upstream_status -eq 0 ]]; then
          testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_PASS
        else
          testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_FAIL
        fi
      else
        runOnServer "${p_name}" "IMAGECI_USER='$IMAGECI_USER' IMAGECI_PWD='$IMAGECI_PWD' bash -s" < ./cerncentostests.sh
        g=$?
        GLOBAL_RETURN=$((GLOBAL_RETURN+g))
        t_CheckExitStatus $g
        if [[ $g -ne 0 ]]; then
          testEnd "${TESTTYPE}-${flavor}" "cern_centos_tests" $TEST_FAIL
          testAddComments "${TESTTYPE}-${flavor}" "cern_centos_tests" "CERN tests failed"
          testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_FAIL
        else
          testEnd "${TESTTYPE}-${flavor}" "cern_centos_tests" $TEST_PASS
          if [[ $upstream_status -eq 0 ]]; then
            testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_PASS
          else
            testEnd "${TESTTYPE}-${flavor}" "summary" $TEST_FAIL
          fi
        fi
      fi

      t_Log "Retrieve cloud-init logs"
      cloudInitLogsRetrieval "${p_name}"

      deleteServer "${p_name}" "${TESTTYPE}-${flavor}" "cern_centos_tests"
    fi

    t_Log "All done with ${p_name}!"
  fi

done

testShowResults "${IMAGE}"
testCreateJUnit
testSendMetrics

# Sleep a bit to make sure Gitlab grabs all the output
sleep 10
exit $GLOBAL_RETURN
