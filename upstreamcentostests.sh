#!/bin/bash
# The following script runs Upstream CentOS functional tests with some tweaks

yum install git jq patch cern-linuxsupport-access -y
RET=$?
if [[ $RET -ne 0 ]]; then
  echo "Failed to install required packages, maybe a problem with the repository configuration?"
  exit $RET
fi

if [[ -z "$1" ]]; then
    # Find the last commit that passed upstream's Jenkins
    centos_ver=$(/usr/bin/rpm -q $(/usr/bin/rpm -qf /etc/redhat-release) --queryformat '%{version}\n'|cut -f 1 -d '.')
    PASSING=$(curl -s https://ci.centos.org/job/CentOS-Core-QA-t_functional-c${centos_ver}-64/lastSuccessfulBuild/api/json | jq -r '.actions[] | select(.lastBuiltRevision) | .lastBuiltRevision.SHA1')
    PASSING=${PASSING:-master}
else
    PASSING=$1
fi

cern-linuxsupport-access enable
git clone https://gitlab.cern.ch/linuxsupport/centos_functional_tests.git
cd centos_functional_tests
BRANCH=$(git reset --hard $PASSING)
echo -e "\e[1m\e[32m$BRANCH\e[0m"
BRANCH=$(git log -1)
echo -e "\e[1m\e[32m$BRANCH\e[0m"

# Disable certain tests from upstream
# We will add CERN specific tests on https://gitlab.cern.ch/linuxsupport/testing/cern_centos_functional_tests if needed
cat > ./skipped-tests.list  <<DELIM
# This file contains list of tests we need/want to skip
# Reason is when there is upstream bug that we're aware of
# So this file should contain:
#  - centos version (using $centos_ver)
#  - test to skip (tests/p_${name}/test.sh)
#  - reason why it's actually skipped (url to upstream BZ, or bug report)
# Separated by |
8|tests/p_gzip/30-gzexe-test|https://apps.centos.org/kanboard/project/23/task/833
8|tests/p_diffutils/10-cmp-tests|https://bugzilla.redhat.com/show_bug.cgi?id=1732960
9|tests/p_diffutils/10-cmp-tests|https://bugzilla.redhat.com/show_bug.cgi?id=1732960
7|tests/0_common/00_centos_repos.sh|We want to keep CERN repos enabled
8|tests/0_common/00_centos_repos.sh|We want to keep CERN repos enabled
9|tests/0_common/00_centos_repos.sh|We want to keep CERN repos enabled
7|tests/0_common/20_upgrade_all.sh|Avoid too much noise on CI logs
8|tests/0_common/20_upgrade_all.sh|Avoid too much noise on CI logs
9|tests/0_common/20_upgrade_all.sh|Avoid too much noise on CI logs
7|tests/0_common/30_dns_works.sh|We spawn VM without waiting for DNS register
8|tests/0_common/30_dns_works.sh|We spawn VM without waiting for DNS register
9|tests/0_common/30_dns_works.sh|We spawn VM without waiting for DNS register
7|tests/0_common/50_test_comps.sh|No need to test this here
8|tests/0_common/50_test_comps.sh|No need to test this here
9|tests/0_common/50_test_comps.sh|No need to test this here
9|tests/p_amanda/*|No amanda
9|tests/p_anaconda/*|No Anaconda patch
7|tests/p_arpwatch/*|It is omitted in cc7 anyway
8|tests/p_arpwatch/*|arpwatch always ends up in coredump in c8
9|tests/p_arpwatch/*|No arpwatch
9|tests/p_centos-release/centos-release_centos-base_repos.sh|Changed repo files
7|tests/p_cron/*|Avoid spamming VM owner with cron tests
8|tests/p_cron/*|Avoid spamming VM owner with cron tests
9|tests/p_cron/*|Avoid spamming VM owner with cron tests
9|tests/p_curl/0-install_curl.sh|Curl already installed, curl-minimal conflicts with curl
8|tests/p_dovecot/*|Disable temporarily
9|tests/p_dovecot/*|Disable temporarily
9|tests/p_file/01_file_mime_application.sh|Executable mime has changed
9|tests/p_freeradius/*|Fails on 9
9|tests/p_gzip/*|No ncompress
8|tests/p_httpd/*|httpd results in 403 for all requests, disabling for now
9|tests/p_httpd/*|httpd results in 403 for all requests, disabling for now
7|tests/p_ipa-server/*|No Freeipa default tests, also, we use cern-get-keytab
8|tests/p_ipa-server/*|No Freeipa default tests, also, we use cern-get-keytab
9|tests/p_ipa-server/*|No Freeipa default tests, also, we use cern-get-keytab
9|tests/p_iptables/*|No iptables
7|tests/p_iputils/*|Traceroute can fail for unknown reasons
8|tests/p_iputils/*|Traceroute can fail for unknown reasons
9|tests/p_iputils/*|Traceroute can fail for unknown reasons
9|tests/p_java-openjdk/*|No java-openjdk 1.6
9|tests/p_libxml2-python/*|No python36
9|tests/p_lsb/*|No redhat-lsb in CS9
9|tests/p_mailman/*|No mailman
7|tests/p_mtr/mtr_test.sh|IPv6 fails too often
8|tests/p_mtr/mtr_test.sh|IPv6 fails too often
9|tests/p_mtr/mtr_test.sh|IPv6 fails too often
7|tests/p_ntp/*|We have our own NTP servers, checking in a separate test
8|tests/p_ntp/*|We have our own NTP servers, checking in a separate test
9|tests/p_ntp/*|We have our own NTP servers, checking in a separate test
9|tests/p_net-snmp/*|Not working on 9
9|tests/p_openssh/sshd_user_login-with-key.sh|invalid key length for 1024-length-keys. /etc/crypto-policies/back-ends/openssh.config
9|tests/p_php_*/*|No php 7.2 or 7.3
7|tests/p_postfix/*|No postfix tests are required, avoid spamming node owner
8|tests/p_postfix/*|No postfix tests are required, avoid spamming node owner
9|tests/p_postfix/*|No postfix tests are required, avoid spamming node owner
9|tests/p_postgresql/*|Skipping postgresql tests
9|tests/p_python/*|No python36
9|tests/p_python3-mod_wsgi/*|Expecting python3-mod_wsgi to install httpd. Not in RHEL9.
9|tests/p_python38-mod_wsgi/*|No python38-mod_wsgi
9|tests/p_rsync/*|No xinetd
9|tests/p_ruby/20-ruby-version-test.sh|Doesn't know about ruby 3.0
9|tests/p_ruby/30-irb-version-test.sh|Doesn't know about irb 1.3.5
9|tests/p_ruby/40-ri-version-test.sh|Doesn't know about ruby 3.0
9|tests/p_ruby/50-rdoc-version-test.sh|Doesn't know about ruby 3.0
9|tests/p_screen/*|Screen comes from EPEL, no EPEL yet
9|tests/p_selinux/*|No libselinux-python
7|tests/p_sendmail/*|No mail tests are required, avoid spamming node owner
8|tests/p_sendmail/*|No mail tests are required, avoid spamming node owner
9|tests/p_sendmail/*|No mail tests are required, avoid spamming node owner
9|tests/p_setup/group_file_test.sh|Test doesn't know CS9's nobody group id
9|tests/p_setup/passwd_file_test.sh|Test doesn't know CS9's nobody group id
7|tests/p_squid/*|squid seems to fail from time to time, disabling for now
8|tests/p_squid/*|squid seems to always fail, disabling for now
9|tests/p_squid/*|squid seems to always fail, disabling for now
9|tests/p_tftp-server/*|No xinetd
7|tests/p_tomcat/*|tomcat_manager_test.sh fails. Excluding (tomcat is not important for us).
9|tests/p_tomcat/*|No Tomcat
7|tests/p_yum-plugin-fastestmirror/*|CERN CentOS does not have mirror list enabled
8|tests/p_yum-plugin-fastestmirror/*|CERN CentOS does not have mirror list enabled
9|tests/p_yum-plugin-fastestmirror/*|CERN CentOS does not have mirror list enabled
7|tests/r_check_mod_packages/*|Does not apply for CCentOS
8|tests/r_check_mod_packages/*|Does not apply for CCentOS
9|tests/r_check_mod_packages/*|Does not apply for CCentOS
8|tests/r_lamp/*|Fails too often
9|tests/r_lamp/*|Fails too often
7|tests/z_rpminfo/*|Does not apply in our case
8|tests/z_rpminfo/*|Does not apply in our case
9|tests/z_rpminfo/*|Does not apply in our case
7|tests/z_repoclosure/*|No need to test this here
8|tests/z_repoclosure/*|No need to test this here
9|tests/z_repoclosure/*|No need to test this here
DELIM

DISTRO_NAME=$(sed '/^NAME/!d; s/.*\"\(.*\)\"/\1/' /etc/os-release)
DISTRO_VERSION=$(sed '/^VERSION_ID/!d; s/.*\"\(.*\)\"/\1/' /etc/os-release)

if [[ ${DISTRO_VERSION%%.*} -eq 9 ]]; then
  # Install initscripts-service for RHEL/AlmaLinux 9
  dnf install -y initscripts-service
fi

# Disable obvious CentOS jobs for elc distros ...
if [ "$DISTRO_NAME" != "CentOS Linux" ]; then
  # Performs tests on /etc/centos-release and friends
  echo "7|tests/p_centos-release/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_centos-release/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/p_centos-release/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "7|tests/p_lsb/lsb_release_brand_test*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_lsb/lsb_release_brand_test*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/p_lsb/lsb_release_brand_test*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "7|tests/p_httpd/httpd_centos_brand_*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_httpd/httpd_centos_brand_*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/p_httpd/httpd_centos_brand_*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "7|tests/p_lynx/lynx_default_page_centos_test.sh|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_lynx/lynx_default_page_centos_test.sh|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/p_lynx/lynx_default_page_centos_test.sh|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  # Checks if the kernel secure boot signing key is 'CentOS'
  echo "7|tests/p_kernel/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_kernel/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/p_kernel/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "7|tests/p_shim/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_shim/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/p_shim/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_grub2/01_grub2_secureboot_signed.sh|Incorrect certificate" >> ./skipped-tests.list
  echo "9|tests/p_grub2/01_grub2_secureboot_signed.sh|Incorrect certificate" >> ./skipped-tests.list
  # Uses /etc/centos-release as an input file
  echo "7|tests/r_pdf/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/r_pdf/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/r_pdf/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
fi
# Most CentOS tests work for RHEL as well, with a few exceptions ...
if [ "$DISTRO_NAME" == "Red Hat Enterprise Linux Server" ]; then
  # random failures, ignoring
  echo "7|tests/p_abrt/abrt_gpg_keys*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "7|tests/p_anaconda/anaconda_centos_patch*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "7|tests/p_httpd/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "7|tests/p_java-openjdk/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "7|tests/p_shim/01_shim_secureboot_signed*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "7|tests/p_yum/yum_bugtracker*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "7|tests/p_yum/yum_distroverpkg*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
fi
if [ "$DISTRO_NAME" == "Red Hat Enterprise Linux" ]; then
  echo "8|tests/p_lftp/10_lftp_http_test*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/p_lftp/10_lftp_http_test*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_logwatch/logwatch_test.sh|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_mtr/mtr_test*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/p_mtr/mtr_test*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_httpd/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/p_httpd/*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "8|tests/p_wget/wget_test*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
  echo "9|tests/p_wget/wget_test*|Not applicable on $DISTRO_NAME" >> ./skipped-tests.list
fi

# Tweaks for test suite to work on CERN CentOS
# Need to tweak Pam so passwd tests do not fail due to test user being taken under Kerberos auth
sed -i '/password\s*sufficient\s*pam_krb5.so\s*use_authtok/d' /etc/pam.d/system-auth &> /dev/null

# Don't try to read external resources, we may not have network connection
patch -p0 --ignore-whitespace --no-backup-if-mismatch <<'DELIM'
--- tests/p_curl/curl_test.sh    2020-07-21 19:33:04.136367594 +0200
+++ tests/p_curl/curl_test.sh    2020-07-21 19:34:24.078892598 +0200
@@ -6,8 +6,8 @@


 if [ $SKIP_QA_HARNESS -eq 1 ]; then
-  CHECK_FOR="The CentOS Project"
-  URL="http://www.centos.org/"
+  CHECK_FOR="CERN"
+  URL="http://linux.cern.ch/"
 else
   CHECK_FOR="Index of /srv"
   URL="http://repo.centos.qa/srv/CentOS/"
DELIM
# Exit if we couldn't patch the tests
[[ $? -eq 0 ]] || exit 1

find tests -type f -name "*.sh" -print0 | xargs -0 sed -i "s/ci.centos.org/linuxsoft.cern.ch/g"
find tests -type f -name "*.sh" -print0 | xargs -0 sed -i "s/mirror.centos.org/linuxsoft.cern.ch\/centos/g"

# Stop some failures due to the undefined $CONTAINERTEST variable
export CONTAINERTEST=0

./runtests.sh
