#!/bin/bash

# Results constants
declare -r TEST_SKIPPED=0
declare -r TEST_RUNNING=1
declare -r TEST_ERROR=2
declare -r TEST_FAIL=3
declare -r TEST_PASS=4

# Test results
declare TESTS=()

function t_Log {
  echo -e "[+] $(date) -> $*"
}

function t_BoldGreen {
  t_Log "\e[1m\e[32m$*\e[0m"
}

function t_BoldRed {
  t_Log "\e[1m\e[31m$*\e[0m"
}

function t_CheckExitStatus {
  [ $1 -eq 0 ] && { t_BoldGreen "PASS"; return 0; }
  t_BoldRed "FAIL"
  return 1
}

function waitFor() {
  local MAX_ATTEMPTS=$1
  shift
  local POLLING_INTERVAL=$1
  shift
  local ATTEMPT_COUNTER=0

  while [[ $ATTEMPT_COUNTER -lt $MAX_ATTEMPTS ]]; do
    ATTEMPT_COUNTER=$(($ATTEMPT_COUNTER+1))
    # Run the user's command
    $@
    RET=$?

    case $RET in
      0)
        # 0 for success
        return 0
        ;;
      2)
        # 2 for canceled
        return 2
        ;;
      3)
        # 3 for failed
        return 3
        ;;
      4)
        # 4 for pending/still running
        ;;
    esac

    sleep $POLLING_INTERVAL
  done

  echo "Max attempts reached."
  # 1 for timeout
  return 1
}

function runOnServer() {
  local NAME="$1"
  shift
  # For Puppet, we connect using kerberos. This guarantees that at least the first couple of Puppet runs are complete
  if [[ "$PUPPET" == "true" ]]; then
    local AUTH="-o PreferredAuthentications=gssapi-with-mic"
  else
    local AUTH="-o PreferredAuthentications=publickey -i imageci.pem"
  fi
  ssh -oConnectTimeout=10 -oStrictHostKeyChecking=no $AUTH root@"${NAME}" $*
  return $?
}

function cloudInitLogsRetrieval() {
  local NAME="$1"
  shift

  # Try getting cloud init logs one way or another if bootstrapping failed
  local SSH="ssh -4 -oConnectTimeout=10 -oStrictHostKeyChecking=no"
  local AUTH1="-o PreferredAuthentications=publickey -i imageci.pem"
  local AUTH2="-o PreferredAuthentications=gssapi-with-mic"

  # RHEL 7 stores cloud init info on /var/log/messages directly
  if [[ "$OS_DISTRO" == "RHEL" && $OS_MAJOR -eq 7 ]]; then
    CLOUD_INIT_EXTRACT_CMD="grep 'cloud-init: ' /var/log/messages"
  else
    # Try getting cloud init logs from the default location
    CLOUD_INIT_EXTRACT_CMD="cat /var/log/cloud-init-output.log"
  fi

  # Let's see if we can get in with the SSH key
  if $SSH $AUTH1 root@"${NAME}" "uname -a; uptime; ip addr show dev eth0" 2>/dev/null; then
    t_Log "Able to connect to ${NAME} with SSH key."
    local AUTH="$AUTH1"
  # If not, try with kerberos
  elif $SSH $AUTH2 root@"${NAME}" "uname -a; uptime; ip addr show dev eth0" 2>/dev/null; then
    t_Log "Able to connect to ${NAME} with Kerberos."
    local AUTH="$AUTH2"
  else
    t_BoldRed "Could not connect to ${NAME} with any method."

    t_Log "Trying to see if there's something listening on port 22."
    echo | nc -v -w 30 "${NAME}" 22
    t_Log "Trying to ping the machine to see if it's up."
    ping -c1 "${NAME}"

    return 1;
  fi

  # Get the cloud init logs
  $SSH $AUTH root@"${NAME}" "${CLOUD_INIT_EXTRACT_CMD}" > "${NAME}-cloud-init-output.log"
  [ $? -eq 0 ] && t_BoldGreen "Extracted cloud-init logs" || t_BoldRed "Failed to extract cloud-init logs"
  # Get /var/log/messages
  $SSH $AUTH root@"${NAME}" "cat /var/log/messages" > "${NAME}-messages.log"
  [ $? -eq 0 ] && t_BoldGreen "Extracted /var/log/messages" || t_BoldRed "Failed to extract /var/log/messages"
}

# Machine Learning Anomaly Detection for the Common Person
function checkCommonErrors() {
  local NAME="$1"
  shift

  # Don't bother if we don't have a cloud init log
  [ -f "${NAME}-cloud-init-output.log" ] || return

  t_Log "Checking for common errors in ${NAME}-cloud-init-output.log"

  # Look for failure to run distro-sync
  if grep -q "^Distro-sync not configured" "${NAME}-cloud-init-output.log"; then
    t_BoldRed "Possible distro-sync issue detected in ${NAME}-cloud-init-output.log:"
    grep -B 5 "^Distro-sync not configured" "${NAME}-cloud-init-output.log"
  fi

  # Look for failure to run puppet
  if grep -q "Generating initial puppet.conf (5200/1800)" "${NAME}-cloud-init-output.log"; then
    t_BoldRed "Puppet run interval modified, Kerberos access will probably not work."
  fi

  # Look for a general step failure
  if grep -q "Step '[^$]*' failed." "${NAME}-cloud-init-output.log"; then
    t_BoldRed "Possible step failure detected in ${NAME}-cloud-init-output.log:"
    grep -B 5 "Step '[^$]*' failed." "${NAME}-cloud-init-output.log"
  fi

  # Look for IPv6 error contacting Puppet masters
  if grep -q "Could not retrieve catalog from remote server:" "${NAME}-cloud-init-output.log"; then
    t_BoldRed "Possible IPv6 issue detected in ${NAME}-cloud-init-output.log:"
    grep -B 4 -A 2 "Could not retrieve catalog from remote server:" "${NAME}-cloud-init-output.log"
    t_Log "See: https://cern.service-now.com/service-portal?id=ticket&n=INC2872657"
  fi
  if grep -q "Could not retrieve catalog from remote server:" "${NAME}-messages.log"; then
    t_BoldRed "Possible IPv6 issue detected in ${NAME}-messages.log:"
    grep -B 4 -A 2 "Could not retrieve catalog from remote server:" "${NAME}-messages.log"
    t_Log "See: https://cern.service-now.com/service-portal?id=ticket&n=INC2872657"
  fi
}

function isServerUp() {
  runOnServer "$1" "uname -a; uptime; cat /etc/redhat-release" 2> /dev/null
  [ $? -eq 0 ] && return 0

  return 4
}

# Test format
#  flavor;name;starttime;endtime;status;comments

function testUpdate {
  local FLAVOR="$1"
  local NAME="$2"
  local STARTTIME="$3"
  local ENDTIME="$4"
  local STATUS="$5"
  local COMMENTS="$6"

  for i in ${!TESTS[@]}; do
    local flavor="$(echo "${TESTS[$i]}" | cut -d';' -f1)"
    local name="$(echo "${TESTS[$i]}" | cut -d';' -f2)"

    if [[ "$FLAVOR" == "$flavor" && "$NAME" == "$name" ]]; then
      local start="$(echo "${TESTS[$i]}" | cut -d';' -f3)"
      local end="$(echo "${TESTS[$i]}" | cut -d';' -f4)"
      local status="$(echo "${TESTS[$i]}" | cut -d';' -f5)"
      local comments="$(echo "${TESTS[$i]}" | cut -d';' -f6-)"

      [[ "$STARTTIME" == "undef" ]] && STARTTIME="$start"
      [[ "$ENDTIME"   == "undef" ]] && ENDTIME="$end"
      [[ "$STATUS"    == "undef" ]] && STATUS="$status"
      [[ "$COMMENTS"  == "undef" ]] && COMMENTS="$comments"

      TESTS[$i]="$FLAVOR;$NAME;$STARTTIME;$ENDTIME;$STATUS;$COMMENTS"
      return
    fi
  done

  [[ "$STARTTIME" == "undef" ]] && STARTTIME=""
  [[ "$ENDTIME"   == "undef" ]] && ENDTIME=""
  [[ "$STATUS"    == "undef" ]] && STATUS=""
  [[ "$COMMENTS"  == "undef" ]] && COMMENTS=""

  TESTS+=("$FLAVOR;$NAME;$STARTTIME;$ENDTIME;$STATUS;$COMMENTS")
}

function testGet {
  local FLAVOR="$1"
  local NAME="$2"

  for i in ${!TESTS[@]}; do
    local flavor="$(echo "${TESTS[$i]}" | cut -d';' -f1)"
    local name="$(echo "${TESTS[$i]}" | cut -d';' -f2)"

    if [[ "$FLAVOR" == "$flavor" && "$NAME" == "$name" ]]; then
      echo "${TESTS[$i]}"
    fi
  done
}

function testGetResult {
  local FLAVOR="$1"
  local NAME="$2"

  echo "$(testGet "$FLAVOR" "$NAME")" | cut -d';' -f5
}

function testCreate {
  local FLAVOR="$1"
  local NAME="$2"

  testUpdate "$FLAVOR" "$NAME" undef undef $TEST_SKIPPED undef
}

function testStart {
  local FLAVOR=$1
  local NAME=$2
  local START=$(date +%s)

  testUpdate "$FLAVOR" "$NAME" "$START" undef $TEST_RUNNING undef
}

function testEnd {
  local FLAVOR=$1
  local NAME=$2
  local STATUS=$3
  local END=$(date +%s)

  testUpdate "$FLAVOR" "$NAME" undef "$END" "$STATUS" undef
}

function testAddComments {
  local FLAVOR=$1
  shift
  local NAME=$1
  shift
  local COMMENTS="${*//;}" # remove ; from the comment, reserved char

  COMMENTS="$(echo "${COMMENTS}" | tr "\n" ";")"
  local result="$(testGet "$FLAVOR" "$NAME")"

  local current_comment="$(echo "$result" | cut -d';' -f6-)"
  # If we already had a comment, concatenate the new one with a ;
  [[ ! -z "$current_comment" ]] && COMMENTS="$current_comment;$COMMENTS"

  testUpdate "$FLAVOR" "$NAME" undef undef undef "$COMMENTS"
}

function testClearComments {
  local FLAVOR=$1
  local NAME=$2

  testUpdate "$FLAVOR" "$NAME" undef undef undef ""
}

function testShowResults {
  t_Log "Finished tests of image '\e[1m${1}\e[0m'."
  t_Log "Here are the results:"

  for i in ${!TESTS[@]}; do
    local flavor="$(echo "${TESTS[$i]}" | cut -d';' -f1)"
    local name="$(echo "${TESTS[$i]}" | cut -d';' -f2)"
    local start="$(echo "${TESTS[$i]}" | cut -d';' -f3)"
    local end="$(echo "${TESTS[$i]}" | cut -d';' -f4)"
    local status="$(echo "${TESTS[$i]}" | cut -d';' -f5)"
    local comments="$(echo "${TESTS[$i]}" | cut -d';' -f6-)"

    case $status in
      $TEST_SKIPPED)
        result="SKIPPED" ;;
      $TEST_PASS)
        result="\e[1m\e[32mPASS\e[0m" ;;
      $TEST_ERROR)
        result="\e[31mERROR\e[0m" ;;
      $TEST_FAIL)
        result="\e[1m\e[31mFAIL\e[0m" ;;
    esac

    t_Log " ${flavor} ${name} -> ${result}"

    # Print comments indented, one per line
    while IFS= read -r LINE; do
        [[ ! -z "$LINE" ]] && t_Log "   $LINE"
    done <<< "$(echo "${comments}" | tr ";" "\n")"

  done
}

function testCreateJUnit() {
  mkdir "_junit"
  local XML="_junit/result.xml"
  echo "<?xml version='1.0' encoding='UTF-8'?>" > $XML
  echo "<testsuites>" >> $XML

  for i in ${!TESTS[@]}; do
    local flavor="$(echo "${TESTS[$i]}" | cut -d';' -f1)"
    local name="$(echo "${TESTS[$i]}" | cut -d';' -f2)"
    local start="$(echo "${TESTS[$i]}" | cut -d';' -f3)"
    local end="$(echo "${TESTS[$i]}" | cut -d';' -f4)"
    local status="$(echo "${TESTS[$i]}" | cut -d';' -f5)"
    local comments="$(echo "${TESTS[$i]}" | cut -d';' -f6-)"

    if [[ -z "$start" || -z "$end" ]]; then
      local time=0
    else
      local time=$(($end-$start))
    fi

    echo '<testsuite name="test" tests="">' >> $XML
    echo "<testcase classname='${flavor}' name='${name}' time='${time}'>" >> $XML

    if [[ $status -eq $TEST_SKIPPED ]]; then
      echo '<skipped message="Not tested" />' >> $XML
    elif [[ $status -eq $TEST_ERROR ]]; then
      echo -n '<error><![CDATA[' >> $XML
      # Print comments indented, one per line
      while IFS= read -r LINE; do
          [[ ! -z "$LINE" ]] && echo "$LINE" >> $XML
      done <<< "$(echo "${comments}" | tr ";" "\n")"
      echo ']]></error>' >> $XML
    elif [[ $status -eq $TEST_FAIL ]]; then
      echo -n '<failure><![CDATA[' >> $XML
      # Print comments indented, one per line
      while IFS= read -r LINE; do
          [[ ! -z "$LINE" ]] && echo "$LINE" >> $XML
      done <<< "$(echo "${comments}" | tr ";" "\n")"
      echo ']]></failure>' >> $XML
    fi

    echo '</testcase></testsuite>' >> $XML

  done

  echo '</testsuites>' >> $XML
}

function testSendMetrics() {
  local RESULT="result.json"

  echo "[" > $RESULT

  for i in ${!TESTS[@]}; do
    local flavor="$(echo "${TESTS[$i]}" | cut -d';' -f1)"
    local name="$(echo "${TESTS[$i]}" | cut -d';' -f2)"
    local start="$(echo "${TESTS[$i]}" | cut -d';' -f3)"
    local end="$(echo "${TESTS[$i]}" | cut -d';' -f4)"
    local status="$(echo "${TESTS[$i]}" | cut -d';' -f5)"
    local comments="$(echo "${TESTS[$i]}" | cut -d';' -f6-)"
    local hwflavor="$(echo "${flavor}" | cut -d'-' -f2-)"

    if [[ -z "$start" || -z "$end" ]]; then
      local time=0
      end="$(date +%s)"
    else
      local time=$(($end-$start))
    fi

    # This is kind of dirty, it might break if the cloud team renames their flavors
    if [[ $hwflavor == m2.* ]]; then
      local type="virtual"
    else
      local type="physical"
    fi

    local status_text=$(case "$status" in
      $TEST_SKIPPED) echo "skipped";;
      $TEST_RUNNING) echo "running";;
      $TEST_ERROR)   echo "error";;
      $TEST_FAIL)    echo "failed";;
      $TEST_PASS)    echo "passed";;
    esac)

    echo "{ \"producer\": \"linuxsupport\", \"type\": \"linux-ci\", \"timestamp\": \"${end}\", " >> $RESULT
    echo " \"idb_tags\": [\"os_distro\", \"os_major\", \"os\", \"arch\", \"configuration\", \"machine_type\", \"pipeline_source\", \"job\", \"test_name\", \"flavor\"], " >> $RESULT
    echo " \"os_distro\": \"${OS_DISTRO}\", \"os_major\": \"${OS_MAJOR}\", \"os\": \"${OS_DISTRO}${OS_MAJOR}\", \"arch\": \"${ARCH}\", \"configuration\": \"${TESTTYPE}\", " >> $RESULT
    echo " \"machine_type\": \"${type}\", \"job\": \"${CI_JOB_NAME}\", \"url\": \"${CI_JOB_URL}\", \"pipeline_source\": \"${CI_PIPELINE_SOURCE}\", " >> $RESULT
    echo " \"test_name\": \"${name}\", \"flavor\": \"${hwflavor}\", \"duration_secs\": ${time}, \"status\": ${status}, \"status_text\": \"${status_text}\" } " >> $RESULT
    echo "," >> $RESULT # comma separator goes on it's own line so we can remove it from the last element of the list

  done

  # Remove the last comma (last line of the file)
  sed -i '$ d' $RESULT
  echo "]" >> $RESULT

  local retry=0
  local STATUS=0
  while [[ $retry -lt 3 ]]; do
    local STATUS=$(curl --silent -o /dev/null \
      -w "%{http_code}\n" \
      -H "Content-Type: application/json" \
      --data @$RESULT \
      http://monit-metrics.cern.ch:10012)

    if [[ $STATUS -eq "200" ]]; then
      t_Log "Metrics sent successfully"
      break
    else
      t_Log "Error sending metrics, return code was ${STATUS}"
    fi
    retry=$((retry+1))
    sleep 5
  done
}
